<?php include 'partials/startup.php' ?>

<?php
  // import functions
  include 'partials/functions/getPfpLink.php';
  include 'partials/functions/markdown.php';

  if (isset($_GET['logout'])) {
    include 'partials/login.php';
    exit();
  }

  // Save edited content
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if ($_POST['modifyAction'] == "save") {
      // process emoji and markdown
      $content = $_POST['content'];
      $content = htmlentities($content);
      $content = applyEmoji($content);

      // create new note or edit note
      if ($_POST['isNewPost'] == "true") {
    
        // create new note
        $db->set('INSERT INTO notes (title, content, createdBy, lastEdited) VALUES (:title, :content, :createdBy, :lastEdited)',
          ['title' => htmlentities($_POST['title']), 'content' => $content, 'createdBy' => $_SESSION['username'], 'lastEdited' => date('Y-m-d\TH:i:s')]);
      } else {
        // update note
        $db->set('UPDATE notes SET title=:title, content=:content,lastEdited=:lastEdited WHERE id=:id', 
          ['id' => $_POST['postId'], 'title' => $_POST['title'], 'content' => $content, 'lastEdited' => date('Y-m-d\TH:i:s')]);
      }

      // reload page (to remove post-request content (if user tries to reload)), displays just created post (newest of user)
      header("Location: .?id={$db->getColumn("SELECT id FROM notes WHERE createdBy=? ORDER BY lastEdited DESC LIMIT 1", [$_SESSION['username']])}");
    }

    // share note
    if ($_POST['modifyAction'] == "share") {
      // this script updates the database for the share table per given note
      $id = $_POST['postId'];

      $author = $db->getColumn("SELECT createdBy FROM notes WHERE id=?", [$id]);

      // delete all old shares for this note (this is inefficient, but it makes it simple and robust)
      $db->set("DELETE FROM sharedUsersNote WHERE noteId=?", [$id]);

      // create new shares
      foreach ($_POST['people'] as $person)
          echo "Creating entry for $person " . (($db->set("INSERT INTO sharedUsersNote (noteId, username) VALUES (?, ?);", [$id, $person]) == 1) ? "succeeded" : "failed") . "<br>";

      // reload page (to remove post content (if user tries to reload))
      header("Location: .?id={$_POST['postId']}");
    }
  }

  // delete note
  if (isset($_GET['deleteNote'])) {
    $noteToDeleteId = $_GET['deleteNote'];
    
    // can you delete this note?
    if($db->getColumn("SELECT createdBy FROM notes WHERE id=?", [$noteToDeleteId]) == $_SESSION['username']) {
      echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Deleted Note '{$db->getColumn("SELECT title FROM notes WHERE id='$noteToDeleteId'")}'!'});});</script>";
      $db->set("DELETE FROM sharedUsersNote WHERE noteId=?", [$noteToDeleteId]);
      $db->set("DELETE FROM notes WHERE id=?", [$noteToDeleteId]);
    } else {
      // you can not delete notes that are not your own
      echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'You can only delete notes that you created!'});});</script>";
    }

    // tell the browser to remove the GET param from the URL bar
    echo "<script>history.pushState({}, '', '.');</script>";
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <title>AntNotes</title>

    <?php include 'partials/head.php' ?>

    <link rel="stylesheet" href="css/_pages.css">
  </head>

  <body>
    <div id="wrapper">
      <nav class="hide-on-large-only">
        <div class="nav-wrapper">
          <a href="#" class="brand-logo left">AntNotes</a>
          <ul id="nav-mobile" class="right">
            <li><a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a></li>
          </ul>
        </div>
      </nav>

      <ul id="slide-out" class="sidenav sidenav-fixed z-depth-5">
        <li><h4 class="sidebarTitle">AntNotes</h4></li>
        <?php
          $cmd = $db->get("SELECT * from notes WHERE createdBy=? OR id IN (SELECT noteId from sharedUsersNote WHERE username=?) ORDER BY lastEdited DESC", [$_SESSION['username'], $_SESSION['username']]);
          while ($row = $cmd->fetch()) {
            // check if is selected
            $active = "";
            if (isset($_GET['id']) and $row['id'] == $_GET['id']) {
              $active = "class='active'";
            }

            // check if this is a shared note, or a not shared note
            if ($row['createdBy'] == $_SESSION['username']): ?>
              <!-- This post was created by the current user -->
              <li <?= $active ?> class="truncate">
                <a href='?id=<?= $row['id'] ?>' class="truncate">
                  <?= $row['title'] ?>
                  <!-- Check if this note is shared with anyone, and if yes, then display a 'shared' symbol -->
                  <?php if ($db->getColumn("SELECT COUNT(*) FROM sharedUsersNote WHERE noteId={$row['id']}") != 0): ?>
                  <i class="material-icons right tooltipped" data-position="top" data-tooltip="This note has been shared with other users" style="margin-right: -20px">share</i>
                  <?php endif; ?>
                </a>
              </li>
              <?php else: ?>
              <!-- This post was created by someone else and was shared with this user -->
              <li $active>
                <a class='secondary-content tooltipped' data-position="top" data-tooltip="Shared by <?= $row['createdBy'] ?>" style="height: 3.25em">
                  <img src="<?= getPfpLink($row['createdBy']) ?>" style="height: 3.25em">
                </a>
                <a href='?id=<?= $row['id'] ?>'><?= $row['title'] ?></a>
              </li>
            <?php endif;
          }
        ?>
      </ul>

      <main>
        <?php
          if (isset($_GET['edit']) || isset($_GET['new'])) {
            include 'partials/editor_edit.php';
          } else {
            include 'partials/editor_view.php';
          }
        ?>
      </main>
    </div>

    <?php include 'partials/scripts.html' ?>
    <script src="js/_home.js"></script>
  </body>
</html>