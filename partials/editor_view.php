<?php
    // set variables to be filled by SQL-Query
    $title = "Error";
    $content = "Error";
    $noNotes = true;
    // check there is an id selected first though, otherwise select post 0 (which does not exist)
    $id = isset($_GET['id']) ? $_GET['id'] : 0;

    
    
    // get data from post
    $cmd = $db->get("SELECT * FROM notes WHERE id=?", [$id]);
    while ($row = $cmd->fetch()) {
        $title = $row['title'];
        $content = $row['content'];
        $id = $row['id'];

        $noNotes = false;
    }
?>

<div class="row">
    <?php if ($noNotes) { ?>

    <div class="noNotes">
        <i class="material-icons"><?= $db->getColumn("SELECT ui_welcomeIcon FROM users WHERE username='{$_SESSION['username']}'") ?></i>
    </div>

    <?php } else { ?>

    <div class="col s12">
        <h2><?= $title; ?></h2>
    </div>
    <div class="col s12">
        <?php
            foreach (explode("\n", applyMarkdown($content)) as $brokenUpRow) {
                echo "<p>$brokenUpRow</p>";
                
            }
        ?>
    </div>

    <?php } ?>
</div>



<!-- Share Modal -->
<div id="shareModal" class="modal">
    <div class="modal-content">
        <h4>Share Note</h4>
        
        <form action="." method="post" id="shareForm">
            <input type="hidden" name="modifyAction" value="share">
            <input type='hidden' name='postId' value='<?= $id ?>'>
            <div class="input-field col s12 m6">
                <select name="people[]" class="icons" multiple>
                    <optgroup label="People to share this note with">

                        <?php
                            $cmd = $db->get("SELECT * FROM users WHERE username!=?", [$_SESSION['username']]);
                            while ($row = $cmd->fetch()):
                                $selected = ($db->getColumn("SELECT count(*) FROM sharedUsersNote WHERE noteId=$id AND username='" . $row['username'] . "'") == 1) ? "selected" : "";
                        ?>

                        <option <?= $selected ?> value="<?= $row['username'] ?>" data-icon="<?= getPfpLink($row['username']) ?>" class="left"><?= $row['firstname'] ?> <?= $row['lastname'] ?> (<?= $row['username'] ?>)</option>

                        <?php endwhile; ?>

                    </optgroup>
                </select>
            </div>
        </form>

    </div>
    <div class="modal-footer">
        <a onclick="document.getElementById('shareForm').submit()" class="modal-close waves-effect waves-green btn-flat">Save</a>
    </div>
</div>



<script>
    document.addEventListener('DOMContentLoaded', function() {
        M.FormSelect.init(document.querySelectorAll('select'), {});
        M.Modal.init(document.querySelectorAll('.modal'), {});
    });
</script>


<!-- Floating Button -->
<div class="fixed-action-btn">
    <a id="floatingButton" class="btn-floating btn-large" href="<?php 
        if ($id != 0) {
            echo "?edit&id=$id";
        } else {
            echo "?new";
        }
    ?>">
        <i class="material-icons">mode_edit</i>
    </a>
    <ul>
        <li><a class="btn-floating tooltipped" data-position="left" data-tooltip="Logout" href=".?logout"><i class="material-icons">exit_to_app</i></a></li>
        <li><a class="btn-floating tooltipped" data-position="left" data-tooltip="Settings" href="settings.php?page=account"><i class="material-icons">settings</i></a></li>
        <li><a class="btn-floating tooltipped modal-trigger" data-position="left" data-tooltip="Share this note" href="#shareModal"><i class="material-icons">share</i></a></li>
        <li><a class="btn-floating tooltipped" data-position="left" data-tooltip="Create new note" href=".?new"><i class="material-icons">add</i></a></li>
    </ul>
</div>


<!-- First Run Welcome -->
<?php 
    if($db->getUserColumn("firstLogin") == 1):
        $db->set("UPDATE users SET firstLogin=0 WHERE username=?", [$_SESSION['username']]);
?>

<div class="tap-target blue-grey darken-4" data-target="floatingButton">
    <div class="tap-target-content">
        <h5>Welcome to AntNotes</h5>
        <p>This is your action button, press it to edit your currently selected note, or hover over it to show more actions.</p>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        M.TapTarget.init(document.querySelectorAll('.tap-target'), {});
        M.TapTarget.getInstance(document.getElementsByClassName('tap-target')[0]).open()

        M.Tooltip.init(document.querySelectorAll('.tooltipped'), {});
    });
</script>

<?php endif; ?>