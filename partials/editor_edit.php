<?php
    $title = "";
    $content = ""; 
    $id = "";   

    if (isset($_GET['edit'])) {
        // get data from post
        $cmd = $db->get("SELECT * FROM notes WHERE id=?", [$_GET['id']]);
        while ($row = $cmd->fetch()) {
            $title = $row['title'];
            $content = $row['content'];
            $id = $row['id']; 
        }  
    }
?>

<link rel="stylesheet" href="css/_editor.css">

<div class="row">
    <form action="." method="post" id="postEditForm">
        <input type="hidden" name="modifyAction" value="save">
        <?php if (isset($_GET['new'])) { echo "<input type='hidden' name='isNewPost' value='true'>"; }?>
        <?php if (isset($_GET['edit'])) { echo "<input type='hidden' name='postId' value='$id'>"; }?>

        <div class="col s12">
            <input type="text" name="title" id="title" placeholder="Note Title" value="<?= $title; ?>">
        </div>
        <div class="col s12">
            <textarea name="content" id="content" cols="30" rows="10" placeholder="Note Content"><?= $content; ?></textarea>
        </div>
    </form>
</div>

<div class="fixed-action-btn">
    <a class="btn-floating btn-large tooltipped" data-position="left" data-tooltip="Save" href="#save" onclick="document.getElementById('postEditForm').submit()">
        <i class="material-icons">save</i>
    </a>
    <ul>
        <li><a class="btn-floating tooltipped" data-position="left" data-tooltip="Delete" href=".?deleteNote=<?= $id ?>"><i class="material-icons">delete</i></a></li>
    </ul>
</div>