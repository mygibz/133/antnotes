<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        include 'partials/functions/createUser.php';

        // create user and react to error/success
        if (createUser($_POST['username'], $_POST['firstname'], $_POST['lastname'], $_POST['password'], true))
            header('Location: .');
        else
            header('Location: .?error');
        exit;

        // Set session to be automatically logged in
        $_SESSION['loggedIn'] = true;
        $_SESSION['username'] = $_POST['username'];

    } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        // give error message
        if (isset($_GET['error'])) {
            echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Something went wrong, please check your inputs :('});});</script>";
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>AntNotes - Install</title>

        <?php include 'partials/head.php' ?>
        <link rel="stylesheet" href="css/_login.css">
    </head>

    <body>
    <div id="wrapper">
        <div style="height:25vh"></div>
            <div class="row">
                <div class="col s0 m2"></div>
                <div class="col s12 m8">
                    <div class="card z-depth-5 center" style="max-width: 960px; margin: auto;">
                        <div class="card-content row">
                            <h2 style="margin-top: 5px">Install AntNotes</h2>
                            <form action="" method="post">
                                <div class="row" style="color: var(--light)">
                                    <div class="col s12">
                                        <ul class="tabs">
                                            <li class="tab col s4" id="stage1"><a class="active" href="#welcome">Welcome</a></li>
                                            <li class="tab col s4 disabled" id="stage2"><a href="#admin">Create Root user</a></li>
                                            <li class="tab col s4 disabled" id="stage3"><a href="#finish">FInalize</a></li>
                                        </ul>
                                    </div>


                                    <div id="welcome" class="col s12"><br>
                                        <p>
                                            Welcome to AntNotes This Install wizard will guide you through the installation.
                                        </p>
                                        <br>
                                        <p>
                                            If you encounter any problems, place message me directly at <a href="mailto:confused@ant.lgbt">confused@ant.lgbt</a>, or create an issue on the repo you downloaded this application from.
                                        </p>
                                        <br>
                                        <a class="btn waves-effect waves-light right" onclick="goToStage2()">
                                            Continue
                                            <i class="material-icons right">chevron_right</i>
                                        </a>
                                    </div>


                                    <div id="admin" class="col s12"><br>
                                        <p>
                                            We will start by defining a root user (this will most likely be you).
                                        </p><br>
                                        <div class="input-field col s12">
                                            <input name="username" id="username" type="text" class="requiredInAdminStage invalid" oninput="checkLength(this, true, 25, 5);validateContinueStage3()">
                                            <label for="username">Username (Between 5 and 25 characters)</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <input name="firstname" id="first_name" type="text" class="requiredInAdminStage invalid" oninput="checkLength(this, true, 64, 3);validateContinueStage3()">
                                            <label for="first_name">First Name (Between 3 and 64 characters)</label>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <input name="lastname" id="last_name" type="text" class="requiredInAdminStage invalid" oninput="checkLength(this, true, 64, 3);validateContinueStage3()">
                                            <label for="last_name">Last Name (Between 3 and 64 characters)</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input name="password" id="password" type="password" class="requiredInAdminStage invalid" oninput="checkLength(this, false, 1024, 12);validateContinueStage3()">
                                            <label for="password">Password (At least 12 characters)</label>
                                        </div>
                                        <br>
                                        <a class="btn waves-effect waves-light right disabled" onclick="goToStage3()" id="continueInAdminStageButton">
                                            Continue
                                            <i class="material-icons right">chevron_right</i>
                                        </a>
                                    </div>


                                    <div id="finish" class="col s12">
                                        <div class="col s12">
                                            <br>
                                            <p>
                                                Your Application should now be ready to go, press install to finish the installation. Please change your pfp right after the installation in the settings, as you will be assigned the standart pfp
                                            </p>
                                            <br>
                                            <button class="btn waves-effect waves-light right" type="submit" name="action">Install
                                                <i class="material-icons right">download</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
                <div class="col s0 m2"></div>
            </div>
        </div>

        <?php include 'partials/scripts.html' ?>
        <script src="js/_install.js"></script>
        <script src="js/utils.js"></script>
    </body>
</html>