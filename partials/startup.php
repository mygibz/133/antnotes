<?php
	include 'partials/database.php';

	// start connection
	$db = new Database;

	// check if installed (check if at least one user is entered)
	if (!$db->getColumn("SELECT count(*) FROM users")) {
		include 'partials/install.php';
		exit;
	}

	// start session
	session_start();

	// check if installed (check if at least one user is entered)
	if (!isset($_SESSION['loggedIn']) || $_SESSION['loggedIn'] == false) {
		include 'partials/login.php';
		exit;
	}
?>