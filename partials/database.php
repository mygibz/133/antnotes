<?php
    class Database {
        private $pdo;

        // Enter the login details for your database
        private $hostname = "127.0.0.1";
        private $dbname = "AntNotes";
        private $username = "AntNotesApp";
        private $password = "helloWorldasdjsadfopuaspudf";

        // connect when creating
        public function __construct() {
            $this->pdo = new PDO("mysql:host=$this->hostname;dbname=$this->dbname", $this->username, $this->password);
        }

        // get a column from an sql command
        public function getColumn(string $query, array $args = null) {
            $cmd = $this->pdo->prepare($query);

            // execute with, or without statements
            if (isset($args))
                $cmd->execute($args);
            else
                $cmd->execute();
            
            return $cmd->fetchColumn();
        }

        // get a column from the curent user
        public function getUserColumn(string $column) {
            $cmd = $this->pdo->prepare("SELECT " . $column . " FROM users WHERE username=?");

            $cmd->execute([$_SESSION['username']]);
            
            return $cmd->fetchColumn();
        }

        // execute a given sql-command
        public function set(string $sql, array $args = null) {
            $cmd = $this->pdo->prepare($sql);

            // execute with, or without statements
            if (isset($args))
                $cmd->execute($args);
            else
                $cmd->execute();
        }

        // execute a given query
        public function get(string $query, array $args = null) {
            $cmd = $this->pdo->prepare($query);

            // execute with, or without statements
            if (isset($args))
                $cmd->execute($args);
            else
                $cmd->execute();
            
            return $cmd;
        }

        // return sqldump string
        public function getDump() {            
            return shell_exec("mysqldump --user={$this->username} --password={$this->password} --host={$this->hostname} {$this->dbname}");
        }
    }
?>