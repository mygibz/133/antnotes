<meta charset="UTF-8">
<meta name="description" content="AntNotes, an amazing note taking application">
<meta name="keywords" content="HTML, CSS, JavaScript, Material">
<meta name="author" content="Yanik Ammann">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="css/materialize.css">

<?php
    if (isset($_SESSION['username'])) {
        echo "<link rel='stylesheet' href='css/colors/{$db->getUserColumn("ui_theme")}.css'>";
        echo "<link rel='stylesheet' href='css/colors/accent/{$db->getUserColumn("ui_accent")}.css'>";
    } else {
        echo "<link rel='stylesheet' href='css/colors/dark.css'>";
        echo "<link rel='stylesheet' href='css/colors/accent/blue.css'>";
    }
?>

<link rel="stylesheet" href="css/font.css">

<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/sidebar.css">

<link rel="shortcut icon" href="logo.png" type="image/png">