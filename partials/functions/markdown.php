<!-- Source: me (https://git.ant.lgbt/ConfusedAnt/jrnlweb/-/blob/master/partial/markdown.php) -->

<?php
    function applyMarkdown($content) {
        $lines = explode("\n", $content);

        foreach ($lines as $key => $line) {

            // italic and bold
            $lines[$key] = preg_replace('/\*\*\*(.*?)\*\*\*/', '<b><i>$1</i></b>', $lines[$key]);
            $lines[$key] = preg_replace('/___(.*?)___/', '<b><i>$1</i></b>', $lines[$key]);
            
            // bold
            $lines[$key] = preg_replace('/\*\*(.*?)\*\*/', '<b>$1</b>', $lines[$key]);
            $lines[$key] = preg_replace('/__(.*?)__/', '<b>$1</b>', $lines[$key]);
            
            // italic
            $lines[$key] = preg_replace('/\*(.*?)\*/', '<i>$1</i>', $lines[$key]);
            $lines[$key] = preg_replace('/_(.*?)/', '<i>$1</i>', $lines[$key]);
            
            // strikethrough
            $lines[$key] = preg_replace('/\~\~(.*?)\~\~/', '<s>$1</s>', $lines[$key]);

            // special characters
            $lines[$key] = str_replace("(c)", "&copy;", $lines[$key]);
            $lines[$key] = str_replace("(C)", "&copy;", $lines[$key]);
            $lines[$key] = str_replace("(r)", "&reg;", $lines[$key]);
            $lines[$key] = str_replace("(R)", "&reg;", $lines[$key]);
            $lines[$key] = str_replace("(tm)", "&trade;", $lines[$key]);
            $lines[$key] = str_replace("(TM)", "&trade;", $lines[$key]);
            $lines[$key] = str_replace("(p)", "&sect;", $lines[$key]);
            $lines[$key] = str_replace("(P)", "&sect;", $lines[$key]);
            $lines[$key] = str_replace("+-", "&plusmn;", $lines[$key]);

            // links
            $lines[$key] = preg_replace('/\[(.+)\]\((.+)\)/', '<a href=$2>$1</a>', $lines[$key]);
        }

        return implode("\n", $lines);
    } 

    function applyEmoji($content) {
        $content = str_replace(":grinning:", "😀", $content);
        $content = str_replace(":grinning_face:", "😀", $content);
        
        $content = str_replace(":smile:", "😄", $content);
        $content = str_replace(":smiling:", "😄", $content);
        $content = str_replace(":smiley_face:", "😄", $content);
        $content = str_replace(":happy:", "😄", $content);
        $content = str_replace(":happy_face:", "😄", $content);

        $content = str_replace(":joy:", "😂", $content);
        $content = str_replace(":lol:", "😂", $content);
        $content = str_replace(":laughing:", "😂", $content);
        $content = str_replace(":laughing_tears:", "😂", $content);
        $content = str_replace(":laughing_crying:", "😂", $content);

        $content = str_replace(":sob:", "😭", $content);
        $content = str_replace(":sobbing_face:", "😭", $content);

        $content = str_replace(":hug:", "🤗", $content);
        $content = str_replace(":hugs:", "🤗", $content);
        $content = str_replace(":hugging:", "🤗", $content);
        $content = str_replace(":hugging_face:", "🤗", $content);

        $content = str_replace(":wink:", "😉", $content);
        $content = str_replace(":wink_face:", "😉", $content);
        $content = str_replace(":winky_face:", "😉", $content);
        $content = str_replace(":winking_face:", "😉", $content);

        $content = str_replace(":kiss:", "😘", $content);
        $content = str_replace(":kissing:", "😘", $content);
        $content = str_replace(":blowing_kiss:", "😘", $content);
        $content = str_replace(":blow_a_kiss:", "😘", $content);
        $content = str_replace(":face_blowing_kiss:", "😘", $content);
        $content = str_replace(":face_throwing_kiss:", "😘", $content);

        $content = str_replace(":tongue:", "👅", $content);

        $content = str_replace(":tongue_out:", "😝", $content);
        $content = str_replace(":stuck_out_tongue_closed_eyes:", "😝", $content);
        
        $content = str_replace(":crazy:", "😜", $content);
        $content = str_replace(":wink_tongue:", "😜", $content);
        $content = str_replace(":winking_face_with_tongue:", "😜", $content);
        $content = str_replace(":winking_face_with_stuckOut_tongue:", "😜", $content);
        $content = str_replace(":stuck_out_tongue_winking_eyes:", "😝", $content);

        $content = str_replace(":think:", "🤔", $content);
        $content = str_replace(":thinker:", "🤔", $content);
        $content = str_replace(":thinking:", "🤔", $content);
        $content = str_replace(":thinking_face:", "🤔", $content);

        $content = str_replace(":sweat_smile:", "😅", $content);
        $content = str_replace(":happy_sweat:", "😅", $content);

        $content = str_replace(":heart_smile:", "🥰", $content);
        $content = str_replace(":smiling_face_with_hearts:", "🥰", $content);
        $content = str_replace(":smiling_face_with_3_hearts:", "🥰", $content);
        $content = str_replace(":smiling_face_with_three_hearts:", "🥰", $content);
        
        $content = str_replace(":heart_eyes:", "😍", $content);
        $content = str_replace(":heart_face:", "😍", $content);
        $content = str_replace(":smiling_face_with_heart_eyes:", "😍", $content);
        $content = str_replace(":smiling_face_with_heartShaped_eyes:", "😍", $content);

        $content = str_replace(":gay:", "😍", $content);
        $content = str_replace(":gay_flag:", "😍", $content);
        $content = str_replace(":lgbt_flag:", "😍", $content);
        $content = str_replace(":rainbow_flag:", "😍", $content);
        
        $content = str_replace(":heart:", "❤️", $content);
        $content = str_replace(":red_heart:", "❤️", $content);
        $content = str_replace(":heart_red:", "❤️", $content);

        $content = str_replace(":orange_heart:", "🧡", $content);
        $content = str_replace(":heart_orange:", "🧡", $content);

        $content = str_replace(":yellow_heart:", "💛", $content);
        $content = str_replace(":heart_yellow:", "💛", $content);

        $content = str_replace(":green_heart:", "💚", $content);
        $content = str_replace(":heart_green:", "💚", $content);

        $content = str_replace(":blue_heart:", "💙", $content);
        $content = str_replace(":heart_blue:", "💙", $content);

        $content = str_replace(":purple_heart:", "💜", $content);
        $content = str_replace(":heart_purple:", "💜", $content);

        return $content;
    }
?>