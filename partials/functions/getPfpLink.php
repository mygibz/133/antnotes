<?php
    function getPfpLink($usernameToGetPfpFor = null) {
        global $db;

        // default to current username, if no other username has been definied
        if (!isset($usernameToGetPfpFor)) $usernameToGetPfpFor = $_SESSION['username'];

        // create connection to Database if not already connected
        if (!isset($db)) {
            include 'partials/database.php';
            $db = new Database;
        }

        $pfpCmd = $db->get("SELECT * FROM users WHERE username=?", [$usernameToGetPfpFor]);
        while ($pfpRow = $pfpCmd->fetch()) {
            $output = 'https://bigheads.io/svg?'; // CHANGE URL HERE IF NEEDED
            $output .= "accessory=" . $pfpRow['pfp_accessory'];
            $output .=  '&body=' . $pfpRow['pfp_body'];
            $output .=  '&circleColor=blue';
            $output .=  '&clothing=' . $pfpRow['pfp_clothing'];
            $output .=  '&clothingColor=' . $pfpRow['pfp_clothingColor'];
            $output .=  '&eyebrows=' . $pfpRow['pfp_eyebrows'];
            $output .=  '&eyes=' . $pfpRow['pfp_eyes'];
            $output .=  '&faceMask=' . ($pfpRow['pfp_faceMask'] != "none");
            $output .=  '&faceMaskColor=' . $pfpRow['pfp_faceMask'];
            $output .=  '&facialHair=' . $pfpRow['pfp_facialHair'];
            $output .=  '&graphic=none';
            $output .=  '&hair=' . $pfpRow['pfp_hair'];
            $output .=  '&hairColor=' . $pfpRow['pfp_hairColor'];
            $output .=  '&hat=' . $pfpRow['pfp_hat'];
            $output .=  '&hatColor=' . $pfpRow['pfp_hatColor'];
            // $output .=  '&lashes=' . ($pfpRow['pfp_lashes'] == 1) ? "true" : "false";
            $output .=  '&mask=false';
            $output .=  '&mouth=' . $pfpRow['pfp_mouth'];
            $output .=  '&skinTone=' . $pfpRow['pfp_skinTone'];
            return $output;
        }  
    }
?>