<?php
    if (isset($_GET['ui_accent'])) {
        $db->set("UPDATE users SET ui_accent=? WHERE username=?", [strtolower($_GET['ui_accent']), $_SESSION['username']]);
        header("Location: settings.php?page=theme"); 
    }

    if (isset($_GET['ui_theme'])) {
        $db->set("UPDATE users SET ui_theme=? WHERE username=?", [strtolower($_GET['ui_theme']), $_SESSION['username']]);
        header("Location: settings.php?page=theme"); 
    }
    
    if (isset($_GET['ui_welcomeIcon'])) {
        $db->set("UPDATE users SET ui_welcomeIcon=? WHERE username=?", [strtolower($_GET['ui_welcomeIcon']), $_SESSION['username']]);
        header("Location: settings.php?page=theme"); 
    }
?>

<div class="row" id="content">
    <div class="col s12">
        <h2>Theming</h2>
    </div>
    <div class="col s12">
        <div class="row">

            <?php
                $data = json_decode(file_get_contents('partials/settings/user/theme.json'), true);
                foreach ($data as $group): 
            ?>
                <div class="col s6 m4 l3">
                    <h5><?= $group['title'] ?></h5>
                    <?php foreach ($group['options'] as $value): ?>
                        <p>
                            <label>
                                <input name="<?= $group['dbName'] ?>" type="radio" <?= ($db->GetUserColumn($group['dbName']) == strtolower($value['name'])) ? "checked" : "" ?> onchange="document.location.href='settings.php?page=theme&<?= $group['dbName'] ?>=<?= strtolower($value['name']) ?>'" />
                                <span><?= (isset($value['isIcon']) and $value['isIcon']) ? "<i class='material-icons'>{$value['name']}</i>" : $value['name']; ?></span>
                            </label>
                        </p>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>