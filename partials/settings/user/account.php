<?php
    // import functions
    include 'partials/functions/getPfpLink.php';

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        if (isset($_POST['changePfp'])) {
            $db->set("UPDATE users SET pfp_lashes=:pfp_lashes, pfp_faceMask=:pfp_faceMask, pfp_body=:pfp_body, pfp_lipColor=:pfp_lipColor, pfp_skinTone=:pfp_skinTone, pfp_hair=:pfp_hair, pfp_hairColor=:pfp_hairColor, pfp_clothing=:pfp_clothing, pfp_clothingColor=:pfp_clothingColor, pfp_eyes=:pfp_eyes, pfp_eyebrows=:pfp_eyebrows, pfp_mouth=:pfp_mouth, pfp_facialHair=:pfp_facialHair, pfp_accessory=:pfp_accessory, pfp_hat=:pfp_hat, pfp_hatColor=:pfp_hatColor WHERE username=:username", [
                'username' => $_SESSION['username'],
                'pfp_lashes' => isset($_POST['pfpLashes']),
                'pfp_faceMask' => $_POST['pfpFaceMask'],
                'pfp_body' => $_POST['pfpBody'],
                'pfp_lipColor' => $_POST['pfpLipColor'],
                'pfp_skinTone' => $_POST['pfpSkinTone'],
                'pfp_hair' => $_POST['pfpHair'],
                'pfp_hairColor' => $_POST['pfpHairColor'],
                'pfp_clothing' => $_POST['pfpClothing'],
                'pfp_clothingColor' => $_POST['pfpClothingColor'],
                'pfp_eyes' => $_POST['pfpEyes'],
                'pfp_eyebrows' => $_POST['pfpEyebrows'],
                'pfp_mouth' => $_POST['pfpMouth'],
                'pfp_facialHair' => $_POST['pfpFacialHair'],
                'pfp_accessory' => $_POST['pfpAccessory'],
                'pfp_hat' => $_POST['pfpHat'],
                'pfp_hatColor' => $_POST['pfpHatColor']]);
        }

        if (isset($_POST['changeName'])) {
            $db->set("UPDATE users SET firstname=:firstname, lastname=:lastname WHERE username=:username", [
                'username' => $_SESSION['username'],
                'firstname' => $_POST['first_name'],
                'lastname' => $_POST['last_name']
            ]);
        }

    }

    $cmd = $db->get("SELECT * FROM users WHERE username=?", [$_SESSION['username']]);
    while ($row = $cmd->fetch()) {
        $element = $row; // idk why this is necesairy, but it doesn't work if I don't do this
?>

<div class="row" id="content">
    <div class="col s12">
        <h2>My Account</h2><br>
    </div>
    <div class="col s12">

        <div class="row">
            <div class="col s12">
                <form action="settings.php?page=account" method="post">
                    <input type="hidden" name="changeName">
                    <div class="input-field col s12 m5">
                        <input placeholder="Fist Name" id="first_name" name="first_name" type="text" class="validate" value="<?= $element['firstname'] ?>">
                        <label for="first_name">First name</label>
                    </div>
                    <div class="input-field col s12 m5">
                        <input placeholder="Last Name" id="last_name" name="last_name" type="text" class="validate" value="<?= $element['lastname'] ?>">
                        <label for="last_name">Last name</label>
                    </div>
                    <div class="col m2">
                        <button class="btn waves-effect waves-light bottom" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </form>
            </div>
        </div>


        <h4>Customize Avatar</h4>

        <div class="row">
            <div class="col s12 m4 l3">
                <img src="<?= getPfpLink() ?>">
            </div>

            <form class="col s12 m8 l9" action="settings.php?page=account" method="post" id="pfpForm">
                <input type="hidden" name="changePfp">

                <div class="col s12">
                    <?php
                        $data = json_decode(file_get_contents('partials/settings/user/account.json'), true);
                        foreach ($data as $group) {

                            echo "<div class='input-field col s12 l6'>";
                            echo "<select name='{$group['name']}' onchange=\"document.getElementById('pfpForm').submit()\">";

                            foreach ($group['values'] as $key => $value) {
                                $select = ($element[$group['dbName']] == $value['name']) ? 'selected' : '';
                                $readableName = (isset($value['readableName'])) ? $value['readableName'] : ucwords($value['name']);
                                echo "<option value='{$value['name']}' $select>$readableName</option>";
                            }

                            echo "</select>";
                            echo "<label>{$group['readableName']}</label>";
                            echo "</div>";
                        }
                    ?>
                </div>

            </form>
    <?php } ?>
        </div>
    </div>
</div>

<script>
document.addEventListener('DOMContentLoaded', function() {
    M.FormSelect.init(document.querySelectorAll('select'), {});
});
</script>