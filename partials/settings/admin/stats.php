<?php
    $gitRepoRoot = "https://git.ant.lgbt/api/v4/projects/103";
    
    // import functions
    include 'partials/functions/getPfpLink.php';
?>

<link rel="stylesheet" href="partials/settings/admin/stats.css">

<div class="row" id="content">
    <div class="col s12">
        <h2>Stats</h2>
    </div>
    <div>

        <div class="col s12 m6 l3">
            <div class="card">
                <div class="card-content">
                    <span class="card-title">Users</span>
                    <p>Count: <?= $db->getColumn("SELECT COUNT(*) FROM users") ?></p>
                </div>
                <div class="card-action">
                    <a class="tooltipped btn modal-trigger" href="#listUsersModal" data-position="top" data-tooltip="List all users">
                        <i class="material-icons left">view_list</i>
                        List users
                    </a>
                </div>
            </div>
        </div>
        
        <div class="col s12 m6 l3">
            <div class="card">
                <div class="card-content">
                    <span class="card-title">Notes</span>
                    <p>Count: <?= $db->getColumn("SELECT COUNT(*) FROM notes") ?></p>
                </div>
                <div class="card-action">
                    <a class="tooltipped btn modal-trigger" href="#listNotesModal" data-position="top" data-tooltip="List all notes">
                        <i class="material-icons left">view_list</i>
                        List Notes
                    </a>
                </div>
            </div>
        </div>
        
        <div class="col s12 l6">
            <div class="card">
                <div class="card-content">

                <?php 
                    $localCommit = substr(file_get_contents('.git/refs/heads/master'), 0, 8);
                    $localVersion = shell_exec("git describe --abbrev=0 --tags");
                    $remoteCommit = json_decode(file_get_contents($gitRepoRoot . "/repository/commits"))[0]->short_id;
                    $remoteVersion = json_decode(file_get_contents($gitRepoRoot . "/releases"))[0]->tag_name;
                    if (trim($localCommit) != trim($remoteCommit)): ?>

                    <a class="tooltipped btn modal-trigger right" href="#updateModal" data-position="top" data-tooltip="Pull all changes from remote repository (this might break stuff)">
                        <i class="material-icons left">update</i>
                        Pull changes
                    </a>
                <?php endif; ?>

                    <span class="card-title">Installation</span> <!-- end(scandir('.git/refs/tags/')) -->
                    <p>Installed version: <?= $localVersion ?> (<?= $localCommit ?>)</p>
                    <p>Latest version: <?= $remoteVersion ?> (<?= $remoteCommit ?>)</p>
                </div>
            </div>
        </div>
        
    </div>
</div>

<!-- Update Modal -->
<div id="updateModal" class="modal">
    <div class="modal-content">
        <h4>Updating</h4>

        <p id="updateStatus">Please note, that this might break your install (this feature is currently in beta)</p>

        <div class="progress center-align" id="updateLoadingAnimation" style="display: none;">
            <div class="indeterminate"></div>
        </div>

        <pre id="updateResult" style="display: none;">
            
        </pre>
        
        <div class="modal-footer" id="updateFooter">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
            <a onclick="update()" class="waves-effect waves-green btn-flat">Proceed</a>
        </div>
    </div>
</div>
<script>
    function update() {
        document.getElementById('updateLoadingAnimation').style.display = "";
        document.getElementById('updateFooter').style.display = "none";

        document.getElementById('updateStatus').innerText = "";

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("updateResult").innerHTML = xhttp.responseText;
                document.getElementById('updateResult').style.display = "";
                document.getElementById('updateLoadingAnimation').style.display = "none";

                document.getElementById('updateStatus').innerText = "Please reload to apply changes";
            }
        };
        xhttp.open("GET", "partials/functions/update.php", true);
        xhttp.send();
    }
</script>

<!-- Users modal -->
<div id="listUsersModal" class="modal">
    <div class="modal-content">
        <h4>All users</h4>
        
        <ul class="collapsible">

            <?php
                    $cmd = $db->get("SELECT * FROM users");
                    while ($row = $cmd->fetch()):
            ?>
            <li class="collection-item avatar">
                <div class="collapsible-header">
                    <img src="<?= getPfpLink($row['username']) ?>" alt="" class="circle" class="responsive-img">
                    <div class="onlywrap">
                        <b><?= $row['username'] ?></b>
                        <span><?= $row['firstname'] . " " . $row['lastname'] ?></span>
                    </div>
                </div>

                <div class="collapsible-body">
                    <p>
                        <i class="material-icons left"><?= $row['ui_welcomeIcon'] ?></i>
                        Welcome icon: <?= $row['ui_welcomeIcon'] ?>
                    </p>
                    <p>
                        <i class="material-icons left">supervisor_account</i>
                        Permission level: <?= ($db->getUserColumn('is_administrator') == '1') ? "Administrator" : "User" ?>
                    </p>
                    <p>
                        <i class="material-icons left">style</i>
                        UI theme: <?= $db->getUserColumn("ui_theme") ?>
                    </p>
                    <p>
                        <i class="material-icons left">folder</i>
                        Count of Notes: <?= $db->getColumn("SELECT count(*) FROM notes WHERE createdBy=?", [$row['username']]) ?>
                    </p>
                    <p>
                        <i class="material-icons left">folder_shared</i>
                        Count of received shares: <?= $db->getColumn("SELECT count(*) FROM sharedUsersNote WHERE username=?", [$row['username']]) ?>
                    </p>
                </div>
            </li>
            <?php endwhile; ?>

        </ul>
        
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
</div>
        

<!-- Notes modal -->
<div id="listNotesModal" class="modal">
    <div class="modal-content">
        <h4>All notes</h4>
        
        <ul class="collapsible">

            <?php
                    $cmd = $db->get("SELECT * FROM notes");
                    while ($row = $cmd->fetch()):
            ?>
            <li class="collection-item avatar">
                <div class="collapsible-header" style="cursor:default">
                    <?php
                        $countOfShares = $db->getColumn("SELECT count(*) FROM sharedUsersNote WHERE noteId=?", [$row['id']]);
                        if ($countOfShares == 0) {
                            echo "<i class='medium material-icons tooltipped' data-position='top' data-tooltip='This note is not shared with anyone'>folder</i>";
                        } else {
                            echo "<i class='medium material-icons tooltipped' data-position='top' data-tooltip='This note is shared with $countOfShares users'>folder_shared</i>";
                        }
                        
                    ?>
                    <div class="onlywrap">
                        <b><?= $row['title'] ?></b>
                        <span>Author: <?= $row['createdBy'] ?></span>
                        <span>Last Edited: <span class="tooltipped" data-position='top' data-tooltip='<?= $row['lastEdited'] ?>'><?= date_format(date_create($row['lastEdited']), "jS F Y") ?></span></span>
                    </div>
                </div>
            </li>
            <?php endwhile; ?>

        </ul>
        
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
</div>


<script>
    document.addEventListener('DOMContentLoaded', function() {
        var instances = M.Tooltip.init(document.querySelectorAll('.tooltipped'), {});
        var instances = M.Modal.init(document.querySelectorAll('.modal'), {});
        var instances = M.Collapsible.init(document.querySelectorAll('.collapsible'), {});
    });
</script>