<?php
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {

        if (isset($_GET['error'])) {
            echo "<script>document.addEventListener('DOMContentLoaded', function() {M.toast({html: 'Something went wrong, the user couldnt be added :('});});</script>";
        }

        if (isset($_GET['setBackground'])) {
            $db->set('UPDATE globalSettings SET login_bgPicture=?', [$_GET['setBackground']]);
        }

        if (isset($_GET['setForeground'])) {
            $db->set('UPDATE globalSettings SET login_fgPicture=?', [$_GET['setForeground']]);
        }

        // tell the browser to remove the GET param from the URL bar
        echo "<script>history.pushState({}, '', 'settings.php?page=general');</script>";
    }

    $db_bgImage = $db->getColumn('SELECT login_bgPicture FROM globalSettings');
    $db_fgImage = $db->getColumn('SELECT login_fgPicture FROM globalSettings');
?>

<link rel="stylesheet" href="partials/settings/admin/general.css">

<div class="row" id="content">
    <div class="col s12">
        <h2>General</h2>
        <p>Here you can adjust some general settings about your installation. Please note, that these settings are <b>global</b> (that means they affect every user, not just you).</p>
    </div>
    
    <div>
        <div class="col s12">
            <h4>Login page imagery</h4>

            <div class="col s12">
                <h5>Foreground</h5>
                <?php foreach (array_slice(scandir("img/loginPage/"), 2) as $value): ?>
                    <div class="col s12 m6 l4 radioCards">
                        <div class="card <?= ($value == $db_fgImage) ? "selectedRadioCard" : "" ?>" onclick="document.location.href='settings.php?page=general&setForeground=<?= $value ?>'">
                            <div class="card-image">
                                <img src="img/loginPage/<?= $value; ?>">
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="col s12">
                <h5>Background</h5>
                <?php foreach (array_slice(scandir("img/loginPage/"), 2) as $value): ?>
                    <div class="col s12 m6 l4 radioCards">
                        <div class="card <?= ($value == $db_bgImage) ? "selectedRadioCard" : "" ?>" onclick="document.location.href='settings.php?page=general&setBackground=<?= $value ?>'">
                            <div class="card-image">
                                <img src="img/loginPage/<?= $value ?>">
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>