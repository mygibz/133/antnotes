> Read a formated version of this README on my website: [ant.lgbt/posts/developement/antnotes](https://ant.lgbt/posts/developement/antnotes/)

# AntNotes

A notes application using PHP and SQL. This Webapp has been tested in Chromium, Google Chrome, Mozilla Firefox, Microsoft Edge, Vivaldi and Safari. It also features an installer (basically where you create your first user).

The Projects Repository is located here: https://git.ant.lgbt/school/m133/antnotes. Feel free to fork this app if you wish to do so (although I would like you informing me when you do, I love to see what you make with it). If you want to download a specific older version, or just want to know what has changed since the last time, go to the [releases](https://git.ant.lgbt/school/m133/antnotes/-/releases) tab in the repo to know more, or download snapshots of the source code for that specific version (please note, that the database may not be compatible between versions and may require manual intervention).

## Home Screen

![](img/Screenshot_Main.png)

The home screen is made to look quite simple, it has your current notes on the left, which will display the note when selected. In the bottom right is the Floating Action Button, hover over it to get the options to add a new note, to share the currently selected note, to go to the settings, and to logout.

The Share menu is pretty self-explainatory, click on the select-box and select every user you want to share this note with (ofc, make sure you have at least one other user created).

### Editor

![](img/Screenshot_Edit.png)

This is your editor, here you can add text to your note and even edit the title. On the bottom right you have a different Floating Action button, use this to save the current edits (just exit the page (like clicking on a different note in the sidebar) to not save the current edits). You can also hover over the button, in order to get the ability to delete the note.

## Settings

The setting screen has a lot of options and is pretty fleshed out. You can exit it, by clicking the floating button on the bottom right of the screen. Please also note, that all settings under the *Administration* section are only availabe to Administrators (obviously), you can elevate a user to be an Admin in the *Users* tab. All administration-settings are applied globally (to every user), while the other Settings are user-specific.

### Personal - Account

![](img/Screenshot_Account.png)

On this page, you have a lot of small settings (do NOT panic). First you have your name, feel free to change it in case you got married or just felt like you wanted something different. Please note, that you can't change your username.

Below that, you have your Avatar (something like a profile-picture), this is how you look to other people on AntNotes. Feel free to customize it to your liking with the Select-Boxes.

### Personal - Theming

![](img/Screenshot_Theming.png)

This is a pretty simple screen, you can change your Accent Color, your Site Theme and your Welcome Icon (the icon, that gets displayed on the Home screen when you haven't selected a note). Feel free to try everything and spice things up a bit.

### Administration - General

![](img/Screenshot_General.png)

This is a pretty simple page, just select whatever image you want to be displayed on the Login-Screen.

### Administration - Users

![](img/Screenshot_Users.png)

Here you can see every user in your installation. You can give and take administrator priveleges from them, and delete (Just deletes the user, nothing more) or sweep them (Deletes the user, their notes and shares).

This tab also allows you to create new users, if you wish to do so.

### Administration - Statistics

![](img/Screenshot_Stats_Main.png)

This screen has multiple tiles to show you stats about your installation (more tiles might be added in the future). Both the *Users* and *Notes* tile have a count visible, this is the amount of Users/Notes that are currently in the Database, you can click the button to open a popup with much more info.

The Installation tile shows the currently installed version (works by using git, in order to see get the current stats). You can also see the *Latest version* it pulls this information directly from AntNotes' git repo. If there is an update available, you will get a button on this tile, asking you to update (just executes a *git pull* command, please note that this feature is currently in beta and requires your php to have permission to execute this command).

#### Administration - Stats: Users Popup

![](img/Screenshot_Stats_Users.png)

Here you see each users personal settings. You can also see how many Notes the user has created themselves, and how many of he received.

#### Administration - Stats: Notes Popup

![](img/Screenshot_Stats_Notes.png)

Here you can see all notes created by every user, sorted by date created. The icon on the left shows if the note is being shared or not (hover to see with how many users). You can also see the Title of the note and the Author, as well as the time the note was last edited (hover over this, to get the original timestamp).

### Administration - Export

![](img/Screenshot_Export.png)

This is the last page in the settings, here you see a complete dump of the current state of the Database. You are offered (with the buttons on the top-left, to either copy the entire dump to clipboard, or to download it as an .sql file). This allows you to make backups in case something breaks, or to migrate to a new install. In order to use this dump, just execute this sql, instead of the default one from *Installation*.

PLEASE MAKE SURE TO KEEP THIS EXPORT SECURE, this export contains **all** the data that is stored by AntNotes.

## Installation

Docker command I used:
```bash
docker run --rm -p 80:80 -e LOG_STDOUT=true -e LOG_STDERR=true -e LOG_LEVEL=debug --name AntNotes -v $(pwd):/var/www/html fauria/lamp
```

Also testest by [Samuel](https://gitlab.com/sgart) with XAMPP (PHP version: 7.2)

### Additional Requirements

- mysqldump (for export functionality)
- git for version statistics and update)

If you wish to install AntNotes through a different Method (like Apache), please make sure that you position AntNotes in the root of the server. Subfolders are not currently supported.

### SQL Structure (important!)

Execute this SQL before launching the App:
```sql
-- You can change the Database name here, if you wish (note, that you have to change it in partials/database.php)
CREATE DATABASE AntNotes;
USE AntNotes;

CREATE TABLE notes (
    id int NOT NULL AUTO_INCREMENT,
    title varchar(250),
    content text,
    createdBy varchar(25),
    lastEdited datetime,
    PRIMARY KEY (id)
); 

CREATE TABLE users (
    username varchar(25) NOT NULL,
    firstname varchar(64),
    lastname varchar(64),
    passwd varchar(64),
    is_administrator tinyint(1),
    ui_welcomeIcon varchar(12),
    ui_accent varchar(6),
    ui_theme varchar(5),
    firstLogin tinyint(1),
    pfp_lashes tinyint(1),
    pfp_faceMask varchar(5),
    pfp_body varchar(7),
    pfp_lipColor varchar(8),
    pfp_skinTone varchar(6),
    pfp_hair varchar(7),
    pfp_hairColor varchar(6),
    pfp_clothing varchar(10),
    pfp_clothingColor varchar(5),
    pfp_eyes varchar(10),
    pfp_eyebrows varchar(11),
    pfp_mouth varchar(9),
    pfp_facialHair varchar(11),
    pfp_accessory varchar(12),
    pfp_hat varchar(6),
    pfp_hatColor varchar(6),
    PRIMARY KEY (username)
);

CREATE TABLE sharedUsersNote (
    noteId int,
    username varchar(25)
);

CREATE TABLE globalSettings (
    login_bgPicture varchar(25), 
    login_fgPicture varchar(25)
);

-- change login details if needed (make sure to also change them in partials/database.php)
GRANT ALL ON AntNotes.* TO AntNotesApp@'127.0.0.1' IDENTIFIED BY 'helloWorldasdjsadfopuaspudf';
FLUSH privileges;

-- default settings, feel free to adjust if you wish (although I advice to first make sure that the defaults work)
INSERT INTO globalSettings (login_bgPicture, login_fgPicture) VALUES ('catto.webp', 'forest.jpg');
```

## Customisations

You can change the Root-URL of the BigHead profile pictures in *partials/functions/getPfpLink.php*, if you want to.

### Adding more lockscreen pictures

In order to add more loginpage pictures, just place them into the */img/loginPage/*. Please note, that the filename (including the filextension) can't exceed the length of 25 characters.

### Updating

In order to check if there are newer versions of the app available, please check the Statistics page (visible to admins only). Please note, that that update process requires php to have the permission to run `git pull` inside the directory.

If you want to update this app manually, either just replace all the files yourself (all the data, incl. settings, are saved in the SQL database), or run `git pull` in the project root (the folder where you find this *README.md* and the *.git* folder).

## Sources

**Images**
- [/img/loginPage/forest.jpg](https://pixabay.com/photos/fog-forest-dark-gloomy-fir-forest-1535201/)
- [/img/loginPage/catto.webp](https://pixabay.com/photos/cat-animal-cat-s-eyes-eyes-pet-1285634/)
- [/img/loginPage/sheep.webp](https://unsplash.com/photos/c82zac9l_ss) (from me)
- [/img/loginPage/ship.webp](https://unsplash.com/photos/CdWt_ZF3Fi8) (from me)
- [/img/loginPage/field.webp](https://unsplash.com/photos/37-NS5OEBho) (from me)

**Code**

- [highlight.js](https://highlightjs.org/)
- [MaterializeCSS](https://materializecss.com/)
- Text to Markdown and Emoji converter [Jrnl](https://git.ant.lgbt/ConfusedAnt/jrnlweb/-/blob/master/partial/markdown.php) (from me)