<?php include 'partials/startup.php' ?>
<!DOCTYPE html>
<html>
    <head>
            <title>AntNotes</title>
            
            <?php include 'partials/head.php' ?>

            <link rel="stylesheet" href="css/_pages.css">
            <link rel="stylesheet" href="css/_settings.css">
    </head>

    <body>
        <div id="wrapper">
        <nav class="hide-on-large-only">
                <div class="nav-wrapper">
                    <a href="#" class="brand-logo left">AntNotes</a>
                    <ul id="nav-mobile" class="right">
                        <li><a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a></li>
                    </ul>
                </div>
        </nav>

        <ul id="slide-out" class="sidenav sidenav-fixed z-depth-5">
            <li><h4 class="sidebarTitle">Settings</h4></li>
            <li <?= ($_GET['page'] == "account") ? 'class=\'active\'' : ''; ?>><a href="?page=account">My Account</a></li>
            <li <?= ($_GET['page'] == "theme") ? 'class=\'active\'' : ''; ?>><a href="?page=theme">Theming</a></li>

            <?php
                // Display Administration settings, only if user is an administrator
                $isAdmin = false;
                $cmd = $db->get("SELECT * FROM users WHERE username=? AND is_administrator=1", [$_SESSION["username"]]);
                if ($cmd->fetchAll()) {
                    $isAdmin = true;
                    echo "<li><h5 class='sidebarTitle'>Administration</h5></li>";
                    echo "<li" . (($_GET['page'] == "general") ? ' class=\'active\'' : '') . "><a href='?page=general'>General</a></li>";
                    echo "<li" . (($_GET['page'] == "users") ? ' class=\'active\'' : '') . "><a href='?page=users'>Users</a></li>";
                    echo "<li" . (($_GET['page'] == "stats") ? ' class=\'active\'' : '') . "><a href='?page=stats'>Statistics</a></li>";
                    echo "<li" . (($_GET['page'] == "export") ? ' class=\'active\'' : '') . "><a href='?page=export'>Backup and Export</a></li>";
                }
            ?>

        </ul>

        <main>
            <?php
            switch ($_GET['page']) {
                case 'account':
                    include 'partials/settings/user/account.php';
                    break;
                case 'theme':
                    include 'partials/settings/user/theme.php';
                    break;

                case 'general':
                    if (!$isAdmin) { include 'partials/settings/401.html'; }
                    else { include 'partials/settings/admin/general.php'; }
                    break;
                case 'stats':
                    if (!$isAdmin) { include 'partials/settings/401.html'; }
                    else { include 'partials/settings/admin/stats.php'; }
                    break;
                case 'users':
                    if (!$isAdmin) { include 'partials/settings/401.html'; }
                    else { include 'partials/settings/admin/users.php'; }
                    break;
                case 'export':
                    if (!$isAdmin) { include 'partials/settings/401.html'; }
                    else { include 'partials/settings/admin/export.php'; }
                    break;

                default:
                    include 'partials/settings/404.html';
                    break;
            }
            ?>
        </main>

        <div class="fixed-action-btn">
        <a class="btn-floating btn-large" href=".">
            <i class="material-icons">chevron_left</i>
        </a>
        </div>

        <?php include 'partials/scripts.html' ?>
        <script src="js/_home.js"></script>
    </body>
</html>