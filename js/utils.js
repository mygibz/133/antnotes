function checkLength(element, checkForOnlyCharacters, maxLength, minLength) {
    let charsValid = (checkForOnlyCharacters) ? element.value.match("^[A-Za-z0-9]+$") : true;

    if (element.value.length > maxLength || element.value.length < minLength || !charsValid) {
        // Something is wrong
        element.classList.remove('valid');
        element.classList.add('invalid');
    }
    else {
        // Everything seems to be okay
        element.classList.remove('invalid');
        element.classList.add('valid');
    }
}

function copyElementToClipboard(element) {
    element.select();
    document.execCommand('copy');
    M.toast({html: 'Successfully copied to clipboard'})
 }

function downloadStringAsFile(text) {
    var element = document.createElement('a');

    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', `AntNotes-${(new Date().toISOString()).slice(0, 10)}.sql`);

    // element.style.display = 'none';
    document.body.appendChild(element);        
    element.click();
    document.body.removeChild(element);
}