document.addEventListener('DOMContentLoaded', function() {
    M.FloatingActionButton.init(document.querySelectorAll('.fixed-action-btn'), {});
    M.Sidenav.init(document.querySelectorAll('.sidenav'));

    M.Tooltip.init(document.querySelectorAll('.tooltipped'), {});
});