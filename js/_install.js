document.addEventListener('DOMContentLoaded', function() {
    M.Tabs.init(document.querySelectorAll('.tabs'), {});
});

// Welcome to Root
function goToStage2() {
    // Proceed to next tab
    document.getElementById('stage2').classList.remove("disabled");
    M.Tabs.getInstance(document.getElementsByClassName('tabs')[0]).select('admin')
}

// Root to Final
function goToStage3() {
    // Proceed to next tab
    document.getElementById('stage3').classList.remove("disabled");
    M.Tabs.getInstance(document.getElementsByClassName('tabs')[0]).select('finish')
}



function validateContinueStage3() {
    let everythingCorrect = true;
    document.querySelectorAll('.requiredInAdminStage').forEach(element => {
        if (element.classList.contains('invalid'))
            everythingCorrect = false;
    });

    if (everythingCorrect == true)
        document.getElementById('continueInAdminStageButton').classList.remove('disabled');
    else
        document.getElementById('continueInAdminStageButton').classList.add('disabled');
}